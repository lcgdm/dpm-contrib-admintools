create database user_accounting;
use user_accounting
create table accounts (entry_date DATETIME NOT NULL, gid INT(10) UNSIGNED NOT NULL, uid INT(10) UNSIGNED NOT NULL, filesize BIGINT(20) UNSIGNED NOT NULL, PRIMARY KEY(entry_date, gid, uid));
grant ALL on user_accounting.* to dpminfo ;