#!/bin/env python

# This script "moves" certain amount of files from one spacetoken to another 
#
# Original contribution:
# Tomas Kouba
# Modifications for gridpp toolkit libraries
# Sam Skipsey

import optparse
import MySQLdb
import sys


def print_freeinfo(conn_data, src, dest, verbose):
    """ Prints information about free space in source and destination tokens
        Give advice what can be moved and how.
    """
    try:
        con = MySQLdb.connect(conn_data['dpm_host'], conn_data['dpm_user'], conn_data['dpm_pass'], conn_data['dpm_db'])
        cur = con.cursor()
        cur.execute("select s_token, u_token, g_space, u_space from dpm_space_reserv where u_token=%s", (src))
        srcdata = cur.fetchone()
        cur.execute("select s_token, u_token, g_space, u_space from dpm_space_reserv where u_token=%s", (dest))
        destdata = cur.fetchone()
    except MySQLdb.Error, e:
        print "Error %d: %s" % (e.args[0],e.args[1])
        sys.exit(1)
    print "There is %d (%.2f TiB) assigned space and %d (%.2f TiB) free space for %s." % (
        destdata[2], destdata[2]/1024.0/1024/1024/1024, destdata[3], destdata[3]/1024.0/1024/1024/1024, dest
        )
    
    print "There is %d (%.2f TiB) assigned space and %d (%.2f TiB) free space for %s." % (
        srcdata[2], srcdata[2]/1024.0/1024/1024/1024, srcdata[3], srcdata[3]/1024.0/1024/1024/1024, src
        )
    print "The space can be reassigned to %s like this:" % (dest)
    print "dpm-updatespace --token_desc %s --gspace %d" % (src, srcdata[2] - srcdata[3])
    print "dpm-updatespace --token_desc %s --gspace %d" % (dest, destdata[2] + srcdata[3])
    if con:
        con.close()

def get_conn_data(dpmconfig, nsconfig, verbose):
    """ Returns connection data from DPMCONFIG and NSCONFIG """
    retval = {}

    if verbose:
        print "Getting connection info from %s" % dpmconfig
    dpmconfig_line = open(dpmconfig).readline().strip()
    splitlist = [x.split('/') for x in dpmconfig_line.split('@')]
    retval['dpm_user'] = splitlist[0][0]
    retval['dpm_pass'] = splitlist[0][1]
    retval['dpm_host'] = splitlist[1][0]
    retval['dpm_db'] = splitlist[1][1]
    if verbose:
        print retval

    if verbose:
        print "Getting connection info from %s" % nsconfig
    nsconfig_line = open(nsconfig).readline().strip()
    splitlist = [x.split('/') for x in nsconfig_line.split('@')]
    retval['ns_user'] = splitlist[0][0]
    retval['ns_pass'] = splitlist[0][1]
    retval['ns_host'] = splitlist[1][0]
    retval['ns_db'] = splitlist[1][1]
    if verbose:
        print retval
    return retval    


def move_data(conn_data, src, dest, verbose, dryrun, size):
    """ Move (relabel) given ammount of files in the source space token
        to the destination space token """

    size_change = 0

    if verbose:
        print "Getting setname (internal token id) for source and destination space tokens"
    try:
        con = MySQLdb.connect(conn_data['dpm_host'], conn_data['dpm_user'], conn_data['dpm_pass'], conn_data['dpm_db'])
        cur = con.cursor()
        cur.execute("select s_token, u_token from dpm_space_reserv where u_token=%s", (src))
        srcdata = cur.fetchone()
        cur.execute("select s_token, u_token from dpm_space_reserv where u_token=%s", (dest))
        destdata = cur.fetchone()
    except MySQLdb.Error, e:
        print "Error %d: %s" % (e.args[0],e.args[1])
        sys.exit(1)
    con.close()
    src_setname = srcdata[0]
    dest_setname = destdata[0]
    if verbose:
        print "Source setname: %s, Destination setname: %s" % (src_setname, dest_setname)

    if verbose:
        print "Getting files to move:"
    
    try:
        con = MySQLdb.connect(conn_data['ns_host'], conn_data['ns_user'], conn_data['ns_pass'], conn_data['ns_db'])
        cur = con.cursor()
        cur.execute("""
SELECT
    sfn,
    fm.fileid,
    filesize,
    @total := @total + filesize AS total
FROM
    (cns_db.Cns_file_replica AS fr
JOIN
    cns_db.Cns_file_metadata fm
ON
    fr.fileid=fm.fileid,
    (
        SELECT
            @total := 0) t)
WHERE
    setname=%s
AND @total<%s;
""", (src_setname, size))
        data_to_move = cur.fetchall()

        if verbose:
            print "Files to be moved: "
        for row in data_to_move:
            size_change += int(row[2])
            if verbose:
                print "%s %s" % (row[0], row[2])

        print "Going to move %d files (%d bytes, %.f TiB)" % (len(data_to_move), size_change, size_change/1024.0/1024/1024/1024)

        if dryrun:
            print "Running in dryrun mode - skipping the actual moving"
        else:
            for row in data_to_move:
                pass
                cur.execute("""update Cns_file_replica set setname=%s where fileid=%s""", (dest_setname, row[1]))

        con.commit()
        if not dryrun:
            print "Files 'moved' from %s to %s successfully" % (src, dest)
        cur.close()
    except MySQLdb.Error, e:
        con.rollback()
        print "Error %d: %s" % (e.args[0],e.args[1])
        sys.exit(1)
    con.close()
 
    if dryrun:
        print "Running in dryrun mode - skipping space token size update"
    else:
        try:
            con = MySQLdb.connect(conn_data['dpm_host'], conn_data['dpm_user'], conn_data['dpm_pass'], conn_data['dpm_db'])
            cur = con.cursor()
            cur.execute("select u_space from dpm_space_reserv where u_token=%s", (src))
            oldsize = cur.fetchone()[0]
            cur.execute("update dpm_space_reserv set u_space=%s where u_token=%s", ((int(oldsize)+size_change), src))

            cur.execute("select u_space from dpm_space_reserv where u_token=%s", (dest))
            oldsize = cur.fetchone()[0]
            cur.execute("update dpm_space_reserv set u_space=%s where u_token=%s", ((int(oldsize)-size_change), dest))
            con.commit()
            print "Free space in space tokens updated"
            cur.close()
        except MySQLdb.Error, e:
            con.rollback()
            print "Error %d: %s" % (e.args[0],e.args[1])
            sys.exit(1)
        con.close()


if __name__=="__main__":
    parser = optparse.OptionParser()
    parser.add_option("-v", "--verbose", action="store_true", help="Print information messages about what is being done.")
    parser.add_option("-n", "--dryrun", action="store_true", help="Do not take any destructive action.")
    parser.add_option("-p", "--dpmconfig", action="store", help="Path to DPMCONFIG. File where sql connection info is stored. Default: /usr/etc/DPMCONFIG", default="/usr/etc/DPMCONFIG")
    parser.add_option("-c", "--nsconfig", action="store", help="Path to NSCONFIG. File where sql connection info is stored. Default: /usr/etc/NSCONFIG", default="/usr/etc/NSCONFIG")
    parser.add_option("-s", "--source", action="store", help="Source space token. Files from this token will be 'moved'. Default: ATLASGROUPDISK", default="ATLASGROUPDISK")
    parser.add_option("-d", "--destination", action="store", help="Destination space token. Files will be 'moved' to this token. Default: ATLASDATADISK", default="ATLASDATADISK")
    parser.add_option("-z", "--size", action="store", help="Total size of the files that should be 'moved' from src do dest space token. Default: 1TiB", default="1099511627776")
    
    (options, arguments) = parser.parse_args()

    conn_data = get_conn_data(options.dpmconfig, options.nsconfig, options.verbose)

    intro = "Start info:"
    print "\n%s\n%s" % (intro, "="*len(intro))
    print_freeinfo(conn_data, options.source, options.destination, options.verbose)

    move_data(conn_data, options.source, options.destination, options.verbose, options.dryrun, options.size)
    
    outro = "End info:"
    print "\n%s\n%s" % (outro, "="*len(outro))
    print_freeinfo(conn_data, options.source, options.destination, options.verbose)

# vi: ts=4:et:sw=4
