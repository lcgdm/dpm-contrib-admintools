%if 0%{?rhel} <= 5
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}
%endif

Name:		dpm-contrib-admintools
Version:	0.2.5
Release:	1%{?dist}
Summary:	DPM administration toolkit (contrib from GridPP)
Group:		Applications/System
License:	ASL 2.0
URL:		http://www.gridpp.ac.uk/wiki/DPM-admin-tools
# The source of this package was pulled from upstream's vcs. Use the
# following commands to generate the tarball:
#git clone http://gitlab.cern.ch/lcgdm/dpm-contrib-admintools.git
# cd dpm-contrib-admintools && git archive --prefix dpm-contrib-admintools-0.2.5/ tags/ dpm-contrib-admintools_0_2_5  | gzip > dpm-contrib-admintools-0.2.5.tar.gz
Source0:	%{name}-%{version}.tar.gz
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	cmake
BuildRequires:	python2-devel

Requires:	dpm-python%{?_isa} >= 1.8.2
Requires:	MySQL-python%{?_isa}
Requires:	python-paramiko

%description
This package provides a set of additional administration tools for the Disk
Pool Manager (DPM) service.

They provide easy to use commands to perform common sysadmin operations.

%prep
%setup -q -n %{name}-%{version}

%build
%cmake . -DCMAKE_INSTALL_PREFIX=/ -DPYTHON_SITELIB=%{python_sitearch}

make %{?_smp_mflags}

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}

make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/*
%{python_sitearch}/*
%{_datadir}/*
%doc LICENSE README

%changelog
* Mon Jul 15 2019 Andrea Manzi <amanzi@cern.ch> - 0.2.5-1
- dpm-dump enhancements

* Tue Feb 20 2018 Andrea Manzi <amanzi@cern.ch> - 0.2.4-1
- dpm-dbck enhancements

* Fri Feb 24 2017 Andrea Manzi <amanzi@cern.ch> - 0.2.3-1
- dpm-dbck enhancements

* Thu May 21 2015 Andrea Manzi <amanzi@cern.ch> - 0.2.2-1
- added dpm-dbck

* Fri Jul 27 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 0.2.1-1
- Update for new upstream release  

* Wed Jul 18 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.0-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Mon Jun 12 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 0.2.0-5
- Updated dpm python dependency

* Mon Jun 11 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 0.2.0-4
- Added dist tag to release number

* Tue Jun 08 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 0.2.0-3
- Rebuild for fedora push

* Tue Jun 05 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 0.2.0-2
- Up release for proper make and isa dependencies fixes

* Tue May 15 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 0.2.0-1
- Update for new upstream release
- Build moved to cmake from distutils

* Tue Feb 21 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 0.1.2-1
- Initial build
